from django.urls import path
from project import views

app_name = 'project'


urlpatterns = [
    path('dashboard/',views.dashboard,name='dash'),
    path('about/',views.about,name='abo'),
    path('album/',views.album,name='alb'),
    path('contact/',views.contactview,name='con'),
    path('s_account/',views.settings_account,name='account'),
    path('s_basicinfo/',views.settings_basicinfo,name='basic'),
    path('s_education/',views.settings_education,name='education'),
    path('s_interest/',views.settings_interests,name='interest'),
    path('s_password/',views.settings_password,name='password'),
    path('timeline/',views.timeline,name='time'),
    path('uslogin/',views.uslogin,name='login'),
    path('uslogout/',views.uslogout,name='logout'),
    path('find_friend/',views.find_friend,name='find_friend'),
    path('friends/',views.friends,name='friends'),
    path('friend/',views.friend,name='friend'),
    path('follow/',views.follow,name='follow'),
    path('friendRequest/',views.friendRequest,name='friendRequest'),
    path('ConfDel/',views.ConfDel,name='ConfDel'),
    path('messages/',views.messages,name='messages'),
    path('notification/',views.notification,name='notification'),
    path('forgot_password/',views.forgot_password,name='forgot_password'),
    path('hide_notification',views.hide_notification,name="hide_notification"),

    path('user_inbox',views.user_inbox,name="user_inbox"),
    path('user/ajax/message/',views.save_message_ajax,name='ajax_message'),
    path('user/inbox/all/',views.show_message_all,name='show_message'),
    path('user/name/ajax',views.ajax_fun,name='ajax_fun'),
    path('mark_as_read',views.mark_as_read,name='mark_as_read'),
    path('like_unlike',views.like_unlike,name='like_unlike'),
    path('view_likes',views.view_likes,name='view_likes'),
    path('comments',views.comments,name='comments'),

    path('checkn',views.checkn,name="checkn"),
    path('check_user',views.check_user,name="check_user"),
    path('check_request',views.check_request,name="check_request"),

]
    